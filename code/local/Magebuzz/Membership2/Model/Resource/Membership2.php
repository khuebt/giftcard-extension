<?php
 
class Magebuzz_Membership2_Model_Resource_Membership2 extends Mage_Core_Model_Resource_Db_Abstract
{

    protected function _construct()
    {
        $this->_init('magebuzz_membership2/membership2', 'membership2_id');
    }

}