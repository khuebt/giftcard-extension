<?php
echo 'v0.1.2:Tao attribute membership '.get_class($this)."\n <br /> \n";
$installer = new Mage_Customer_Model_Entity_Setup('core_setup');

$installer->startSetup();

$setup = new Mage_Eav_Model_Entity_Setup('core_setup');

$entityTypeId     = $setup->getEntityTypeId('customer');
$attributeSetId   = $setup->getDefaultAttributeSetId($entityTypeId);
$attributeGroupId = $setup->getDefaultAttributeGroupId($entityTypeId, $attributeSetId);
//$setup->removeAttribute( 'customer', 'your_attribute_code_here' );
//$setup->removeAttribute( 'customer', 'foundation' );

    $installer->addAttribute('customer', 'membership', array(
        'input' => 'select',
        'type' => 'text',
        'backend' => '',
        'label' => 'Membership',
        'source' => 'membership/entity_membership',
        'visible' => 1,
        'is_visible' => 1,
        'required' => 0,
        'default' => '0',
        'frontend' => '',
        'unique' => 0,
        'user_defined' => 0,
        'position'  => 99,
    ));

    $setup->addAttributeToGroup(
        $entityTypeId,
        $attributeSetId,
        $attributeGroupId,
        'membership',
        '999'  //sort_order
    );

    $attribute = Mage::getSingleton("eav/config")->getAttribute("customer", "membership");
    $attribute->setData('used_in_forms', array('adminhtml_customer'));
    $attribute->save();
