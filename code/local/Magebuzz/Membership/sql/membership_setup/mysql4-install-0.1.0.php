<?php
/**
 * Created by PhpStorm.
 * User: Khuebt
 * Date: 29/02/16
 * Time: 6:39
 */
/* @var $installer Mage_Core_Model_Resource_Setup */

echo 'Running This installer: '.get_class($this). 'tao bang membership'. "\n <br /> \n";
$installer = $this;

$installer->startSetup();
$installer->run("
        DROP TABLE IF EXISTS {$this->getTable('membership')};
        CREATE TABLE `{$installer->getTable('membership')}` (
        `membership_id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
        `name`  varchar(50) NULL ,
        `description`  varchar(50) NULL ,
        `discount_type` tinyint(2)  NOT NULL default '0',
        `discount_value` DECIMAL(12,2),
        `status` tinyint(2)  NOT NULL default '0',
        PRIMARY KEY (`membership_id`)
        )
        ;
");

$installer->endSetup();