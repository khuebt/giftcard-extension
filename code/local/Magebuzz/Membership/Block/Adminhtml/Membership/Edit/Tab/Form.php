<?php

class Magebuzz_Membership_Block_Adminhtml_Membership_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
//        if (Mage::getSingleton('adminhtml/session')->getMembershipData()) {
//            $data = Mage::getSingleton('adminhtml/session')->getMembershipData();
//            Mage::getSingleton('adminhtml/session')->setMembershipData(null);
//        } elseif (Mage::registry('membership_data')) {
//            $data = Mage::registry('membership_data')->getData();
//        }
        // $form->setValues($data);

        $fieldset = $form->addFieldset('membership_form', array('legend' => Mage::helper('membership')->__('Item information')));
        $fieldset->addField('name', 'text', array(
            'label' => Mage::helper('membership')->__('Name'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'name',
        ));

        $fieldset->addField('description', 'text', array(
            'label' => Mage::helper('membership')->__('Description'),
            'required' => false,
            'name' => 'description',
        ));
        $fieldset->addField('status', 'select', array(
            'label' => Mage::helper('membership')->__('Status'),
            'name' => 'status',
            'values' => array(
                array(
                    'value' => 1,
                    'label' => Mage::helper('membership')->__('Enabled'),
                ),
                array(
                    'value' => 0,
                    'label' => Mage::helper('membership')->__('Disabled'),
                ),
            ),
        ));
        $fieldset->addField('discount_type', 'select', array(
            'label' => Mage::helper('membership')->__('Type'),
            'name' => 'discount_type',
            'values' => array(
                array(
                    'value' => 0,
                    'label' => Mage::helper('membership')->__('Percent'),
                ),
                array(
                    'value' => 1,
                    'label' => Mage::helper('membership')->__('Fixed Amount'),
                ),
            ),
        ));
        $fieldset->addField('discount_value', 'text', array(
            'name' => 'discount_value',
            'label' => Mage::helper('membership')->__('Value Discount'),
            'title' => Mage::helper('membership')->__('Value_Discount'),
            'required' => false,
        ));
        //lay data tu csdl len form edit
        if (Mage::getSingleton('adminhtml/session')->getMembershipData()) {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getMembershipData());
            Mage::getSingleton('adminhtml/session')->setMembershipData(null);
        } elseif (Mage::registry('membership_data')) {
            $form->setValues(Mage::registry('membership_data')->getData());
        }
        return parent::_prepareForm();
    }
}