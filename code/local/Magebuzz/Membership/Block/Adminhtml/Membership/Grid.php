<?php

/**
 * Created by PhpStorm.
 * User: Khuebt
 * Date: 08/03/16
 * Time: 11:07
 */
class Magebuzz_Membership_Block_Adminhtml_Membership_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('membershipGrid');
        $this->setDefaultSort('membership_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('membership/membership')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('membership_id', array(
            'header' => Mage::helper('membership')->__('ID'),
            'align' => 'right',
            'width' => '50px',
            'index' => 'membership_id',
        ));
        $this->addColumn('name', array(
            'header' => Mage::helper('membership')->__('Name'),
            'align' => 'left',
            'index' => 'name',
        ));
        $this->addColumn('description', array(
            'header' => Mage::helper('membership')->__('Description'),
            'align' => 'left',
            'index' => 'description',
        ));
        $this->addColumn('discount_value', array(
            'header' => Mage::helper('membership')->__('Discount Value'),
            'align' => 'left',
            'index' => 'discount_value',
        ));
        $this->addColumn('discount_type', array(
            'header' => Mage::helper('membership')->__('Discount Type'),
            'align' => 'left',
            'width' => '80px',
            'index' => 'discount_type',
            'type' => 'options',
            'options' => array(
                0 => 'Percent',
                1 => 'Fixed amount',
            ),
        ));
        $this->addColumn('status', array(
            'header' => Mage::helper('membership')->__('Status'),
            'align' => 'left',
            'width' => '80px',
            'index' => 'status',
            'type' => 'options',
            'options' => array(
                0 => 'Disabled',
                1 => 'Enabled',
            ),
        ));
        $this->addColumn('action',
            array(
                'header' => Mage::helper('membership')->__('Action'),
                'width' => '100',
                'type' => 'action',
                'getter' => 'getId',
                'actions' => array(
                    array(
                        'caption' => Mage::helper('membership')->__('Edit'),
                        'url' => array('base' => '*/*/edit'),
                        'field' => 'id'
                    )
                ),
                'filter' => false,
                'sortable' => false,
                'index' => 'stores',
                'is_system' => true,
            ));
        $this->addExportType('*/*/exportCsv', Mage::helper('membership')->__('CSV'));
        $this->addExportType('*/*/exportXml', Mage::helper('membership')->__('XML'));
        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('membership_id');
        $this->getMassactionBlock()->setFormFieldName('membership');
        $this->getMassactionBlock()->addItem('delete', array(
            'label' => Mage::helper('membership')->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
            'confirm' => Mage::helper('membership')->__('Are you sure?')
        ));
        $statuses = Mage::getSingleton('membership/status')->getOptionArray();
       // var_dump($statuses);
//        array_unshift($statuses, array('label' => '', 'value' =>
//            ''));
        $this->getMassactionBlock()->addItem('status', array(
            'label' => Mage::helper('membership')->__('Change status'),
            'url' => $this->getUrl('*/*/massStatus', array('_current' => true)),
            'additional' => array(
                'visibility' => array(
                    'name' => 'status',
                    'type' => 'select',
                    'class' => 'required-entry',
                    'label' => Mage::helper('membership')->__('Status'),
                    'values' => $statuses
                )
            )
        ));
        return $this;
    }
}
