<?php
//
//class Magebuzz_Membership_Block_Sales_Ordertotal extends Mage_Sales_Block_Order_Totals
//
//{
//    public function initTotals()
//    {
//        $amount = $this->getParentBlock()->getOrder()->getMembershipDiscount();
//        //$amount=1;
//         var_dump($amount);
//        if (floatval($amount)) {
//            $total = new Varien_Object();
//            $total->setValue($amount);
//            $total->setBaseValue($this->getParentBlock()->getOrder()->getBaseMembershipDiscount());
//            $total->setLabel('Deal');
//            $parent = $this->getParentBlock();
//            $parent->addTotal($total, 'subtotal');
//            return $this;
//        }
//    }
//    public function getOrder(){
//        if(!$this->hasData('order')){
//            $order = $this->getParentBlock()->getOrder();
//            $this->setData('order',$order);
//        }
//        return $this->getData('order');
//    }
//}
class Magebuzz_Membership_Block_Sales_Ordertotal extends Mage_Sales_Block_Order_Totals
{
    public function getMembershipDiscount(){
        $order = $this->getOrder();
        return $order->getMembershipDiscount();
    }

    public function getBaseMembershipDiscount(){
        $order = $this->getOrder();
        return $order->getBaseMembershipDiscount();
    }

    public function initTotals(){
        $amount = $this->getMembershipDiscount();

        $customerId = $this->getOrder()->getData('customer_id');

        $customer = Mage::getModel('customer/customer')->load($customerId);
        $collection = Mage::getModel('membership/membership')->getCollection()
            ->addFieldToSelect('*')
            ->getData();
        foreach ($collection as $collect) {
            if (($customer->getData('membership') == $collect['membership_id']) && $collect['discount_type'] == 0) {
                $name = 'Deal Membership ' . $collect['name'];
                $value = $collect['discount_value'];
                $label = $name.'('.-$value.'%)';
            }if (($customer->getData('membership') == $collect['membership_id']) && $collect['discount_type'] == 1) {
                $label = 'Deal Membership ' . $collect['name'].' (Fixed)';
            }
        }
        if(floatval($amount)){
            $total = new Varien_Object();
            $total->setCode('Membership');
            $total->setValue($amount);
            $total->setBaseValue($this->getBaseMembershipDiscount());
            $total->setLabel($label);
            $parent = $this->getParentBlock();
            $parent->addTotal($total,'subtotal');
        }
    }

    public function getOrder(){
        if(!$this->hasData('order')){
            $order = $this->getParentBlock()->getOrder();
            $this->setData('order',$order);
        }
        return $this->getData('order');
    }
}

