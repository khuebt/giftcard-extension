<?php

class Magebuzz_Membership_Block_Sales_Invoice_Totals extends Mage_Core_Block_Template
{
    public function initTotals()
    {
        $totalsBlock = $this->getParentBlock();//object(Mage_Adminhtml_Block_Sales_Order_Invoice_Totals) la parent block
        $invoice = $totalsBlock->getInvoice();

        $customerId = $invoice->getData('customer_id');
        $customer = Mage::getModel('customer/customer')->load($customerId);
        $collection = Mage::getModel('membership/membership')->getCollection()
            ->addFieldToSelect('*')
            ->getData();
        foreach ($collection as $collect) {
            if (($customer->getData('membership') == $collect['membership_id']) && $collect['discount_type'] == 0) {
                $name = 'Deal Membership ' . $collect['name'];
                $value = $collect['discount_value'];
                $label = $name.'('.-$value.'%)';
            }if (($customer->getData('membership') == $collect['membership_id']) && $collect['discount_type'] == 1) {
                $label = 'Deal Membership ' . $collect['name'].' (Fixed)';
            }
        }

        if ($invoice->getMembershipDiscount()) {
            $totalsBlock->addTotal(new Varien_Object(array(
                'code' => 'code de lam gi',
                'label' => 'Deal',
                'value' => $invoice->getMembershipDiscount(),
            )), 'subtotal');
        }
    }
}
