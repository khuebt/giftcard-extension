<?php

class Magebuzz_Membership_Model_Total_Invoice_Membership extends Mage_Sales_Model_Order_Invoice_Total_Abstract
{
    /**
     * Collect total when create Invoice
     *
     * @param Mage_Sales_Model_Order_Invoice $invoice
     */
    public function collect(Mage_Sales_Model_Order_Invoice $invoice)
    {
        $customerId = $invoice->getOrder()->getData('customer_id');

        $customer = Mage::getModel('customer/customer')->load($customerId);
        $collection = Mage::getModel('membership/membership')->getCollection()
            ->addFieldToSelect('*')
            ->getData();

        foreach ($collection as $collect) {
            if ($customer->getData('membership') == $collect['membership_id']
                && $collect['discount_type'] == 1
                && $collect['status'] == 1
            ) {
                $discount = $collect['discount_value'];
            }
            if ($customer->getData('membership') == $collect['membership_id']
                && $collect['discount_type'] == 0
                && $collect['status'] == 1
            ) {
                $discount = $invoice->getOrder()->getSubtotal() * $collect['discount_value'] / 100;
            };
        }

        $invoice->setMembershipDiscount(-$discount);
        $invoice->setBaseMembershipDiscount(-$discount);
        $invoice->setBaseGrandTotal($invoice->getBaseGrandTotal() - $discount);
        $invoice->setGrandTotal($invoice->getGrandTotal() - $discount);
        return $this;
    }


}