<?php

class Magebuzz_Membership_Model_Total_Address_Membership extends Mage_Sales_Model_Quote_Address_Total_Abstract
{

    public function __construct()
    {
        $this->setCode('membership');
    }

    public function collect(Mage_Sales_Model_Quote_Address $address)
    {
        //var_dump($address->getSubtotal());
        $quote = $address->getQuote();
        if (!$quote->isVirtual() && $address->getAddressType() == 'billing') {
            return $this;
        }


        if (Mage::getStoreConfigFlag('membership/general/active')) {
            $customer = Mage::getModel('customer/customer')
                ->load(Mage::getSingleton('customer/session')->getCustomerId());
            $collection = Mage::getModel('membership/membership')->getCollection()
                ->addFieldToSelect('*')
                ->getData();

            if(!$customer->getData('membership')){
                $discount =0;
            }
            foreach ($collection as $collect) {
                if ($customer->getData('membership') == $collect['membership_id']
                    && $collect['discount_type'] == 1
                    && $collect['status'] == 1
                ) {
                    die('1');
                    $discount = $collect['discount_value'];
                }
                if ($customer->getData('membership') == $collect['membership_id']
                    && $collect['discount_type'] == 0
                    && $collect['status'] == 1
                ) {
                    $discount = $address->getSubtotal() * $collect['discount_value'] / 100;
                    var_dump($discount);
                }
            }

        }


        $address->setBaseMembershipDiscount(-$discount);
        $address->setMembershipDiscount(-$discount);

        $address->setBaseGrandTotal($address->getBaseGrandTotal() - $discount);
        $address->setGrandTotal($address->getGrandTotal() - $discount);

        return $this;
    }

    public function fetch(Mage_Sales_Model_Quote_Address $address)
    {
        $amount = $address->getMembershipDiscount();

        $customer = Mage::getModel('customer/customer')
            ->load(Mage::getSingleton('customer/session')->getCustomerId());
        $collection = Mage::getModel('membership/membership')->getCollection()
            ->addFieldToSelect('*')
            ->getData();
        foreach ($collection as $collect) {
            if ($customer->getData('membership') == $collect['membership_id']) {
                $title = 'Deal Membership '.$collect['name'];
            }
        }
        if ($amount != 0) {
            $address->addTotal(array(
                'code' => $this->getCode(),
                'title' => $title,
                'value' => $amount
            ));
        }

        return $this;
    }
}