<?php

class Magebuzz_Membership_Model_Entity_Membership extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{

    public function getAllOptions()
    {
        $collection = Mage::getModel('membership/membership')
            ->getCollection();
        $collection->addFieldToSelect(array('membership_id', 'name'));
        $collection->addFieldToFilter('status',array("eq"=>'1'));
        $collection->load();
        // $result = array();
        $result[] = array(
            'value' => '',
            'label' => '',
        );
        foreach ($collection as $membership) {
            $result[] = array(
                'value' => $membership->getData('membership_id'),
                'label' => $membership->getData('name'),
               // 'status' => $membership->getData('status')
            );
        }
        return $result;
    }

}
