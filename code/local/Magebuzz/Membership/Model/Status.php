<?php
class Magebuzz_Membership_Model_Status extends Varien_Object
{
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;
    static public function getOptionArray()
    {
        return array(
            self::STATUS_ENABLED => Mage::helper('membership')->__('Enabled'),
            self::STATUS_DISABLED => Mage::helper('membership')->__('Disabled')
        );
    }
    public function updateAttributes($membershipIds, $attrData, $storeId)
    {

        $this->_getResource()->updateAttributes($membershipIds, $attrData, $storeId);
        $this->setData(array(
            'membership_Ids'       => array_unique($membershipIds),
            'attributes_data'   => $attrData,
            'store_id'          => $storeId
        ));


        return $this;
    }
}
