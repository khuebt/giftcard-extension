<?php

/**
 * Created by PhpStorm.
 * User: Khuebt
 * Date: 07/03/16
 * Time: 10:37
 */
class Magebuzz_Membership_Adminhtml_MembershipController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->_initAction();
        $this->renderLayout();
    }

    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('membership/items')//to dam menu dang active
            ->_addBreadcrumb(
                Mage::helper('adminhtml')->__('Items Manager'),
                Mage::helper('adminhtml')->__('Items Manager')
            );
        return $this;
    }

    public function exportCsvAction()
    {
        $fileName = 'membership.csv';
        $content = $this->getLayout()
            ->createBlock('membership/adminhtml_membership_grid')
            ->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    public function exportXmlAction()
    {
        $fileName = 'membership.xml';
        $content = $this->getLayout()
            ->createBlock('membership/adminhtml_membership_grid')
            ->getXml();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    public function editAction()
    {
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('membership/membership')->load($id);
        if ($model->getId() || $id == 0) {
            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
            if (!empty($data)) {
                $model->setData($data);
            }
            //var_dump($data);
            Mage::register('membership_data', $model);
            $this->loadLayout();
            $this->_setActiveMenu('membership/items');
            $this->_addBreadcrumb(
                Mage::helper('adminhtml')->__('ItemManager'),
                Mage::helper('adminhtml')->__('Memership Item Manager'));
            $this->_addBreadcrumb(
                Mage::helper('adminhtml')->__('ItemNews'),
                Mage::helper('adminhtml')->__('Item News'));
            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock('membership/adminhtml_membership_edit'))
                ->_addLeft($this->getLayout()->createBlock('membership/adminhtml_membership_edit_tabs'));
            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('membership')->__('Item does not exist'));
            $this->_redirect('*/*/');
        }
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            $model = Mage::getModel('membership/membership');
            $model->setData($data)
                ->setId($this->getRequest()->getParam('id'));
            //var_dump($this->getRequest()->getParam('id'));die();
            try {
                if ($model->getCreatedTime == NULL || $model->getUpdateTime() == NULL) {
                    $model->setCreatedTime(now())
                        ->setUpdateTime(now());
                } else {
                    $model->setUpdateTime(now());
                }
                $model->save();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('membership')->__('Item was successfully saved')
                );
                Mage::getSingleton('adminhtml/session')->setFormData(false);

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper(c)->__('Unable to find item to save')
        );
        $this->_redirect('*/*/');
    }

    public function deleteAction()
    {
        if ($this->getRequest()->getParam('id') > 0) {
            try {
                $model = Mage::getModel('membership/membership');
                $model->setId($this->getRequest()->getParam('id'))
                    ->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__('Item was successfully deleted')
                );
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }

    public function massDeleteAction()
    {
        $testIds = $this->getRequest()->getParam('membership');
        // param nay <=> param $this->getMassactionBlock()->setFormFieldName('membership');
        //cua function _prepareMassaction() trong block grid.php
        if (!is_array($testIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select '));
        } else {
            try {
                foreach ($testIds as $testId) {
                    $test = Mage::getModel('membership/membership')->load($testId);
                    $test->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__('Total of %d record(s) were successfully deleted', count($testIds))
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    public function massStatusAction()
    {
        $membership_Ids = $this->getRequest()->getParam('membership');
        if (!is_array($membership_Ids)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Hay chon item'));
        } else {
            try {
                foreach ($membership_Ids as $membership_Id) {
                    $membership = Mage::getSingleton('membership/membership')
                        ->load($membership_Id);
                    $membership->setStatus($this->getRequest()->getParam('status'))
                        ->setIsMassupdate(true)
                        ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) were success fully updated', count($membership_Ids))
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }
}