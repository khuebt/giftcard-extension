<?php
class Magebuzz_Giftcard_Model_Status extends Varien_Object
{
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;
    static public function getOptionArray()
    {
        return array(
            self::STATUS_ENABLED => Mage::helper('giftcard')->__('Enabled'),
            self::STATUS_DISABLED => Mage::helper('giftcard')->__('Disabled')
        );
    }
    public function updateAttributes($giftcardIds, $attrData, $storeId)
    {

        $this->_getResource()->updateAttributes($giftcardIds, $attrData, $storeId);
        $this->setData(array(
            'giftcard_Ids'       => array_unique($giftcardIds),
            'attributes_data'   => $attrData,
            'store_id'          => $storeId
        ));


        return $this;
    }
}
