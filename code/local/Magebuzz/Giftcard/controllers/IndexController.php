<?php

class Magebuzz_Giftcard_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
//       //$A= Mage::getStoreConfig('general/locale/timezone');
//        echo Mage::getModel('core/date')->date('Y-m-d H:i:s');
//        var_dump(Mage::getModel('giftcard/template')->getCollection());
//        die();
    }

    public function createAction()
    {
        $this->loadLayout();
        $id = $this->getRequest()->getParams();
        $gift = Mage::getModel('giftcard/giftcard');
        if ($id) {
            $data = $gift->load($id)->getData();
            Mage::register('giftdata', $data);
        }
        $this->renderLayout();

    }


    public function editAction()
    {
        $this->loadLayout();
        $id = $this->getRequest()->getParams();
        $gift = Mage::getModel('giftcard/giftcard');
        if ($id) {
            $data = $gift->load($id)->getData();
            Mage::register('giftdata', $data);
            // var_dump($data);
        }
        $this->renderLayout();

    }

    public function edit3Action(){
        $data = $this->getRequest()->getPost();
        $id = $this->getRequest()->getParam('id');
        $session = Mage::getSingleton('core/session');
        $data2 = array(
            'title'=>$data['title'],
            'amount'=> $data['amount'],
            'status'=> $data['status'],
//            'created_time'=> $data['created_time'],
//            'update_time'=> $data['update_time'],
        );
        $model = Mage::getModel('giftcard/giftcard')->load($id)->addData($data2);
        try {
            $model->setUpdateTime(Mage::getModel('core/date')->date('Y-m-d H:i:s'));
            $model->setId($id)->save();
            $session->addSuccess('Edit sucessfully');

        } catch (Exception $e){
            $session->addError('Edit Error');
        }
        $this->_redirect('giftcard/index/index');
    }

    public function deleteAction()
    {
        $session = Mage::getSingleton('core/session');
        if ($id = $this->getRequest()->getParam('id')) {

            // init model and delete
            $model = Mage::getModel('giftcard/giftcard');
            $model->load($id);
            $model->delete();
        }
        try {
            $model->save();
            $session->addSuccess('Delete sucessfully');
        } catch (Exception $e) {
            $session->addError('Add Error');
        }
        $this->_redirect('giftcard/index/index');
    }


    public function addAction()
    {
        $data = $this->getRequest()->getPost();
        $session = Mage::getSingleton('core/session');
        $giftcard = Mage::getModel('giftcard/giftcard');
        $giftcard->setData('title', $data['title']);
        $giftcard->setData('giftcode', $data['giftcode']);
        $giftcard->setData('amount', $data['amount']);
        $giftcard->setData('status', $data['status']);
//        $giftcard->setData('created_time', $data['created_time']);

//        $giftcard->setData('update_time', $data['update_time']);
// $person->setData($data);
        try {
//            $giftcard->setCreatedTime(Mage::getModel('core/date')->date('D-m-y H:i:s'));
//            $giftcard->setUpdateTime(Mage::getModel('core/date')->date('D-m-y H:i:s'));
            $giftcard->save();
            $session->addSuccess('Add a gift sucessfully');
        } catch (Exception $e) {
            $session->addError('Add Error');
        }
        $this->_redirect('giftcard/index/index');
    }
}