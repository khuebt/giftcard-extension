<?php

class Magebuzz_Giftcard_Adminhtml_TemplateController extends Mage_Adminhtml_Controller_Action
{

    public function indexAction()
    {
        $this->_initAction();
        $this->renderLayout();
    }

    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('giftcard/items')//to dam menu dang active
            ->_addBreadcrumb(
                Mage::helper('adminhtml')->__('Items Manager'),
                Mage::helper('adminhtml')->__('Items Manager')
            );
        return $this;
    }

    public function exportCsvAction()
    {
        $fileName = 'giftcard.csv';
        $content = $this->getLayout()
            ->createBlock('giftcard/adminhtml_template_grid')
            ->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    public function exportXmlAction()
    {
        $fileName = 'giftcard.xml';
        $content = $this->getLayout()
            ->createBlock('giftcard/adminhtml_template_grid')
            ->getXml();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    public function editAction()
    {
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('giftcard/template')->load($id);
        if ($model->getId() || $id == 0) {//neu lay dc id (thi hien thi form edit) hoac id=0 tuc la add new
            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
            if (!empty($data)) {
                $model->setData($data);
            }
            Mage::register('template_data', $model);
//            var_dump($model->getData());die();
            $this->loadLayout();
            $this->_setActiveMenu('giftcard/items');
            $this->_addBreadcrumb(
                Mage::helper('adminhtml')->__('ItemManager'),
                Mage::helper('adminhtml')->__('Template Item Manager'));
            $this->_addBreadcrumb(
                Mage::helper('adminhtml')->__('ItemNews'),
                Mage::helper('adminhtml')->__('Item News'));
            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock('giftcard/adminhtml_template_edit'))
                ->_addLeft($this->getLayout()->createBlock('giftcard/adminhtml_template_edit_tabs'));
            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('giftcard')->__('Item does not exist'));
            $this->_redirect('*/*/');
        }
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            $model = Mage::getModel('giftcard/template');
            $model->setData($data)
                ->setId($this->getRequest()->getParam('id'));
            //var_dump($this->getRequest()->getParam('id'));die();
            try {
//                if ($model->getCreatedTime == NULL || $model->getUpdateTime() == NULL) {
//                    $model->setCreatedTime(now())
//                        ->setUpdateTime(now());
//                } else {
//                    $model->setUpdateTime(now());
//                }
                $model->setCreatedTime(Mage::getModel('core/date')->date('Y-m-d H:i:s'));
                $model->save();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('giftcard')->__('Item was successfully saved')
                );
                Mage::getSingleton('adminhtml/session')->setFormData(false);

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper(c)->__('Unable to find item to save')
        );
        $this->_redirect('*/*/');
    }

    public function deleteAction()
    {
        if ($this->getRequest()->getParam('id') > 0) {
            try {
                $model = Mage::getModel('giftcard/template');
                $model->setId($this->getRequest()->getParam('id'))
                    ->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__('Item was successfully deleted')
                );
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }

    public function massDeleteAction()
    {
        $testIds = $this->getRequest()->getParam('template');
        // param nay <=> param $this->getMassactionBlock()->setFormFieldName('giftcard');
        //cua function _prepareMassaction() trong block grid.php
        if (!is_array($testIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select '));
        } else {
            try {
                foreach ($testIds as $testId) {
                    $test = Mage::getModel('giftcard/template')->load($testId);
                    $test->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__('Total of %d record(s) were successfully deleted', count($testIds))
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    public function massStatusAction()
    {
        $giftcard_Ids = $this->getRequest()->getParam('template');
        if (!is_array($giftcard_Ids)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Hay chon item'));
        } else {
            try {
                foreach ($giftcard_Ids as $giftcard_Id) {
                    $giftcard = Mage::getSingleton('giftcard/template')
                        ->load($giftcard_Id);
                    $giftcard->setStatus($this->getRequest()->getParam('status'))
                        ->setIsMassupdate(true)
                        ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) were success fully updated', count($giftcard_Ids))
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    public function generateAction()
    {
        $id = $this->getRequest()->getParam('id');
        $template_data = Mage::getModel('giftcard/template')->load($id);


        if (isset($template_data)) {
            $tiento = $template_data->getData('giftcode');
            for ($i = 1; $i <= $template_data['number_of_code']; $i++) {
                $giftcode = Mage::helper('giftcard/template')->generateGiftcode($tiento);

                $giftcard = Mage::getModel('giftcard/giftcard');
                $giftcard->setData('title', $template_data['title']);
                $giftcard->setData('giftcode', $giftcode);
                $giftcard->setData('amount', $template_data['amount']);
                $giftcard->setData('status', $template_data['status']);
                $giftcard->setCreatedTime(Mage::getModel('core/date')->date('Y-m-d H:i:s'));
                $giftcard->setUpdateTime(Mage::getModel('core/date')->date('Y-m-d H:i:s'));
                $giftcard->save();
            }
            Mage::getSingleton('adminhtml/session')->addSuccess(
                Mage::helper('giftcard')->__('Total of ' . $template_data['number_of_code'] . ' giftcode(s) was successfully generated')
            );
        }
        $template_data->setData('is_generated',1);
        $template_data->save();
        $this->_redirect('giftcardadmin/adminhtml_giftcard/index');
    }


}