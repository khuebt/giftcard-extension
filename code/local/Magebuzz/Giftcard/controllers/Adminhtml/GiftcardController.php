<?php
class Magebuzz_Giftcard_Adminhtml_GiftcardController extends Mage_Adminhtml_Controller_Action{

    public function indexAction()
    {
        $this->_initAction();
        $this->renderLayout();
    }
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('giftcard/items')//to dam menu dang active
            ->_addBreadcrumb(
                Mage::helper('adminhtml')->__('Items Manager'),
                Mage::helper('adminhtml')->__('Items Manager')
            );
        return $this;
    }
    public function exportCsvAction()
    {
        $fileName = 'giftcard.csv';
        $content = $this->getLayout()
            ->createBlock('giftcard/adminhtml_giftcard_grid')
            ->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    public function exportXmlAction()
    {
        $fileName = 'giftcard.xml';
        $content = $this->getLayout()
            ->createBlock('giftcard/adminhtml_giftcard_grid')
            ->getXml();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    public function editAction()
    {
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('giftcard/giftcard')->load($id);
        if ($model->getId() || $id == 0) {
            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
            if (!empty($data)) {
                $model->setData($data);
            }
            //var_dump($data);
            Mage::register('giftcard_data', $model);
            $this->loadLayout();
            $this->_setActiveMenu('giftcard/items');
            $this->_addBreadcrumb(
                Mage::helper('adminhtml')->__('ItemManager'),
                Mage::helper('adminhtml')->__('giftcard Item Manager'));
            $this->_addBreadcrumb(
                Mage::helper('adminhtml')->__('ItemNews'),
                Mage::helper('adminhtml')->__('Item News'));
            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock('giftcard/adminhtml_giftcard_edit'))
                ->_addLeft($this->getLayout()->createBlock('giftcard/adminhtml_giftcard_edit_tabs'));
            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('giftcard')->__('Item does not exist'));
            $this->_redirect('*/*/');
        }
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            $model = Mage::getModel('giftcard/giftcard');
            $model->setData($data)
                ->setId($this->getRequest()->getParam('id'));

            try {
//                if ($model->getCreatedTime == NULL || $model->getUpdateTime() == NULL) {
//                    $model->setCreatedTime(Mage::getModel('core/date')->date('D-m-y H:i:s'))
//                        ->setUpdateTime(Mage::getModel('core/date')->date('D-m-y H:i:s'));
//                } else {
                    $model->setUpdateTime( Mage::getModel('core/date')->date('Y-m-d H:i:s'));
//                }
                $model->save();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('giftcard')->__('Item was successfully saved')
                );
                Mage::getSingleton('adminhtml/session')->setFormData(false);

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('giftcard')->__('Unable to find item to save')
        );
        $this->_redirect('*/*/');
    }

    public function deleteAction()
    {
        if ($this->getRequest()->getParam('id') > 0) {
            try {
                $model = Mage::getModel('giftcard/giftcard');
                $model->setId($this->getRequest()->getParam('id'))
                    ->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__('Item was successfully deleted')
                );
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }

    public function massDeleteAction()
    {
        $testIds = $this->getRequest()->getParam('giftcard');
        // param nay <=> param $this->getMassactionBlock()->setFormFieldName('giftcard');
        //cua function _prepareMassaction() trong block grid.php
        if (!is_array($testIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select '));
        } else {
            try {
                foreach ($testIds as $testId) {
                    $test = Mage::getModel('giftcard/giftcard')->load($testId);
                    $test->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__('Total of %d record(s) were successfully deleted', count($testIds))
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    public function massStatusAction()
    {
        $giftcard_Ids = $this->getRequest()->getParam('giftcard');
        if (!is_array($giftcard_Ids)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Hay chon item'));
        } else {
            try {
                foreach ($giftcard_Ids as $giftcard_Id) {
                    $giftcard = Mage::getSingleton('giftcard/giftcard')
                        ->load($giftcard_Id);
                    $giftcard->setStatus($this->getRequest()->getParam('status'))
                        ->setIsMassupdate(true)
                        ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) were success fully updated', count($giftcard_Ids))
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }
}