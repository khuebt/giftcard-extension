<?php
 
echo 'Module GiftCard v0.1.0 : tao bang Giftcard_code';
$installer = $this;

$installer->startSetup();
$installer->run("
    DROP TABLE IF EXISTS {$this->getTable('giftcard/giftcard_code')};
    CREATE TABLE `{$this->getTable('giftcard/giftcard_code')}`(
        `code_id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
        `title`  varchar(50) NOT NULL ,
        `giftcode`  varchar(50) NOT NULL ,
        `amount` DECIMAL(12,2),
        `status` tinyint(2)  NOT NULL default '0',
        `created_time` DATETIME NULL,
        `update_time`  DATETIME NULL,
        PRIMARY KEY (`code_id`)
    );
");


$installer->endSetup();