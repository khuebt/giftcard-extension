<?php

echo '0.1.1 add table Template Giftcard' . "\n <br /> \n";
$installer = $this;
$installer->startSetup();
echo get_class($installer->getConnection());
$table = $installer->getConnection()
    ->newTable($installer->getTable('giftcard/giftcard_template'))
    ->addColumn('template_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity' => true,
        'unsigned' => true,
        'nullable' => false,
        'primary' => true,
    ), 'Id')
    ->addColumn('title', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
        'nullable' => false,
    ), 'Title')
    ->addColumn('giftcode', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
        'nullable' => false,
    ), 'giftcode')
    ->addColumn('amount', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
        'nullable' => false,
    ), 'amount')
    ->addColumn('number_of_code', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable' => false,
    ), 'Number of code ')
    ->addColumn('created_time', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable' => true,
        'default' => null,
    ), 'created at ')
    ->addColumn('status', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable' => false,
        'default' => 0,
    ), 'Status')
    ->addColumn('is_generated', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable' => false,
        'default' => 0,
    ), 'Generated');

if ($this->getTable('giftcard/giftcard_template')) {
    $installer->getConnection()->dropTable($this->getTable('giftcard/giftcard_template'));
$installer->getConnection()->createTable($table);
}
$installer->endSetup();