<?php

class Magebuzz_Giftcard_Block_Frontend_Grid extends Mage_Core_Block_Template
{
    public function __construct()
    {
//        die('1');
        parent::__construct();
        $collection = $this->getGiftcardCollection();
        $this->setCollection($collection);
    }

    //prepare layout

    public function getGiftcardCollection()
    {
        $collection = Mage::getModel('giftcard/giftcard')->getCollection();
        return $collection;
    }

    public function _prepareLayout()
    {
        parent::_prepareLayout();
        $pager = $this->getLayout()->createBlock('page/html_pager', 'giftcard.pager')->setCollection($this->getCollection());
        $this->setChild('pager', $pager);
        return $this;
    }

    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }


    public function getStatusLabel($giftcard)
    {
        if ($giftcard->getId()) {
            if ($giftcard->getStatus() == 1)
                return Mage::helper('giftcard')->__('Enabled');
        }
        return Mage::helper('giftcard')->__('Disabled');
    }


}