<?php

class Magebuzz_Giftcard_Block_Frontend_Form extends Mage_Core_Block_Template
{
    public function getAddAction()
    {
        return $this->getUrl('giftcard/index/add');
    }
    public function getEditAction($id){
        return  $this->getUrl('giftcard/index/edit3',array('id'=>$id));
    }
}