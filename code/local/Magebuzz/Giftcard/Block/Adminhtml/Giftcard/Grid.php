<?php

/**
 * Created by PhpStorm.
 * User: Khuebt
 * Date: 08/03/16
 * Time: 11:07
 */
class Magebuzz_Giftcard_Block_Adminhtml_Giftcard_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('giftcardGrid');
        $this->setDefaultSort('giftcard_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('giftcard/giftcard')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('code_id', array(
            'header' => Mage::helper('giftcard')->__('ID'),
            'align' => 'right',
            'width' => '50px',
            'index' => 'code_id',
        ));
        $this->addColumn('title', array(
            'header' => Mage::helper('giftcard')->__('Title'),
            'align' => 'left',
            'index' => 'title',
        ));
        $this->addColumn('giftcode', array(
            'header' => Mage::helper('giftcard')->__('Giftcode'),
            'align' => 'left',
            'index' => 'giftcode',
        ));
        $storeId = (int) $this->getRequest()->getParam('store', 0);
        $store = Mage::app()->getStore($storeId);
        $this->addColumn('amount', array(
            'header' => Mage::helper('giftcard')->__('Amount'),
            'align' => 'left',
            'index' => 'amount',
            'type'=> 'price',
            'currency_code' => $store->getBaseCurrency()->getCode(),
        ));
//        $this->addColumn('discount_type', array(
//            'header' => Mage::helper('membership')->__('Discount Type'),
//            'align' => 'left',
//            'width' => '80px',
//            'index' => 'discount_type',
//            'type' => 'options',
//            'options' => array(
//                0 => 'Percent',
//                1 => 'Fixed amount',
//            ),
//        ));
        $this->addColumn('status', array(
            'header' => Mage::helper('giftcard')->__('Status'),
            'align' => 'left',
            'width' => '80px',
            'index' => 'status',
            'type' => 'options',
            'options' => array(
                0 => 'Disabled',
                1 => 'Enabled',
            ),
        ));
        $this->addColumn('created_time',array(
            'header'=> Mage::helper('giftcard')->__('Created Time'),
            'index' => 'created_time'
        ));
        $this->addColumn('update_time',array(
            'header'=> Mage::helper('giftcard')->__('Update Time'),
            'index' => 'update_time'
        ));

        $this->addColumn('action',
            array(
                'header' => Mage::helper('giftcard')->__('Action'),
                'width' => '100',
                'type' => 'action',
                'getter' => 'getId',
                'actions' => array(
                    array(
                        'caption' => Mage::helper('giftcard')->__('Edit'),
                        'url' => array('base' => '*/*/edit'),
                        'field' => 'id'
                    )
                ),
                'filter' => false,
                'sortable' => false,
                'index' => 'stores',
                'is_system' => true,
            ));
        $this->addExportType('*/*/exportCsv', Mage::helper('giftcard')->__('CSV'));
        $this->addExportType('*/*/exportXml', Mage::helper('giftcard')->__('XML'));
        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('giftcard_id');
        $this->getMassactionBlock()->setFormFieldName('giftcard');
        $this->getMassactionBlock()->addItem('delete', array(
            'label' => Mage::helper('giftcard')->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
            'confirm' => Mage::helper('giftcard')->__('Are you sure?')
        ));
        $statuses = Mage::getSingleton('giftcard/status')->getOptionArray();
       // var_dump($statuses);
//        array_unshift($statuses, array('label' => '', 'value' =>
//            ''));
        $this->getMassactionBlock()->addItem('status', array(
            'label' => Mage::helper('giftcard')->__('Change status'),
            'url' => $this->getUrl('*/*/massStatus', array('_current' => true)),
            'additional' => array(
                'visibility' => array(
                    'name' => 'status',
                    'type' => 'select',
                    'class' => 'required-entry',
                    'label' => Mage::helper('giftcard')->__('Status'),
                    'values' => $statuses
                )
            )
        ));
        return $this;
    }
}
