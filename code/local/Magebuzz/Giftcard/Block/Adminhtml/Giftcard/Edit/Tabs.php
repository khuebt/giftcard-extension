<?php
class Magebuzz_Giftcard_Block_Adminhtml_Giftcard_Edit_Tabs extends  Mage_Adminhtml_Block_Widget_Tabs

{
    public function __construct()
    {
        parent::__construct();
        $this->setId('giftcard_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('giftcard')->__('Giftcard Information'));
    }
    protected function _beforeToHtml(){
        $this->addTab('form_section', array(
            'label' => Mage::helper('giftcard')->__('Giftcard info'),
            'title' => Mage::helper('giftcard')->__('Thong tin item,hover len moi thay'),
            'content' => $this->getLayout()->createBlock('giftcard/adminhtml_giftcard_edit_tab_form')->toHtml(),
        ));
        return parent::_beforeToHtml();
    }
}
