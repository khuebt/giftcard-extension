<?php

class Magebuzz_Giftcard_Block_Adminhtml_Giftcard_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        if (Mage::getSingleton('adminhtml/session')->getGiftcardData()) {
            $data = Mage::getSingleton('adminhtml/session')->getGiftcardData();
            Mage::getSingleton('adminhtml/session')->setGiftcardData(null);
        } elseif (Mage::registry('giftcard_data')) {
            $data = Mage::registry('giftcard_data')->getData();
        }


        $fieldset = $form->addFieldset('giftcard_form', array('legend' => Mage::helper('giftcard')->__('Giftcard information')));
        $fieldset->addField('title', 'text', array(
            'label' => Mage::helper('giftcard')->__('Title'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'title',
        ));

        $fieldset->addField('giftcode', 'text', array(
            'label' => Mage::helper('giftcard')->__('Gift Code'),
            'required' => true,
            'name' => 'giftcode',
        ));
        $fieldset->addField('status', 'select', array(
            'label' => Mage::helper('giftcard')->__('Status'),
            'name' => 'status',
            'values' => array(
                array(
                    'value' => 1,
                    'label' => Mage::helper('giftcard')->__('Enabled'),
                ),
                array(
                    'value' => 0,
                    'label' => Mage::helper('giftcard')->__('Disabled'),
                ),
            ),
        ));
//        $fieldset->addField('discount_type', 'select', array(
//            'label' => Mage::helper('membership')->__('Type'),
//            'name' => 'discount_type',
//            'values' => array(
//                array(
//                    'value' => 0,
//                    'label' => Mage::helper('membership')->__('Percent'),
//                ),
//                array(
//                    'value' => 1,
//                    'label' => Mage::helper('membership')->__('Fixed Amount'),
//                ),
//            ),
//        ));
        $fieldset->addField('amount', 'text', array(
            'name' => 'amount',
            'label' => Mage::helper('giftcard')->__('Amount'),
            'title' => Mage::helper('giftcard')->__('Amount'),
            'required' => true,
        ));
        //lay data tu csdl len form edit
//        if (Mage::getSingleton('adminhtml/session')->getGiftcardData()) {
//            $form->setValues(Mage::getSingleton('adminhtml/session')->getGiftcardData());
//            Mage::getSingleton('adminhtml/session')->setGiftcardData(null);
//        } elseif (Mage::registry('giftcard_data')) {
//            $form->setValues(Mage::registry('giftcard_data')->getData());
//        }
        $form->setValues($data);
        return parent::_prepareForm();
    }
}