<?php

/**
 * Created by PhpStorm.
 * User: Khuebt
 * Date: 01/03/16
 * Time: 14:33
 */
class Magebuzz_Giftcard_Block_Adminhtml_Giftcard_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
        $this->_objectId = 'id';
        $this->_blockGroup = 'giftcard';
        $this->_controller = 'adminhtml_giftcard';

        $this->_updateButton('save', 'label', Mage::helper('giftcard')->__('Save Item'));
        $this->_updateButton('delete', 'label', Mage::helper('giftcard')->__('Delete Item'));

        $this->_addButton('saveandcontinue', array(
            'label' => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick' => 'saveAndContinueEdit()',
            'class' => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('giftcard_content') == null)
                    tinyMCE.execCommand('mceAddControl', false, 'giftcard_content');
                else
                    tinyMCE.execCommand('mceRemoveControl', false, 'giftcard_content');
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";

    }
//    public function getHeaderText()
//    {
//        if (Mage::registry('membership_data')
//            && Mage::registry('membership_data')->getId()
//        ) {
//            return Mage::helper('membership')->__("Edit Membership '%s'", $this->htmlEscape(Mage::registry('membership_data')->getTitle())
//            );
//        }
//        return Mage::helper('membership')->__('Add Item');
//    }
    public function getHeaderText()
    {
        if (Mage::registry('giftcard_data') && Mage::registry('giftcard_data')->getId()
        ) {
            //var_dump(Mage::registry('membership_data'));
            return Mage::helper('giftcard')->__("Edit Item '%s'", $this->htmlEscape(Mage::registry('giftcard_data')->getName()));
        } else {
            return Mage::helper('giftcard')->__('Add Item');
        }
    }
}
