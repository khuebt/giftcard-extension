<?php
class Magebuzz_Giftcard_Block_Adminhtml_Template extends Mage_Adminhtml_Block_Widget_Grid_Container{
    public function __construct(){
        $this->_controller = 'adminhtml_template';
        $this->_blockGroup = 'giftcard';//wtf
        $this->_headerText = Mage::helper('giftcard')->__('Template Manager');
        $this->_addButtonLabel = Mage::helper('giftcard')->__('Add New Template');
        parent::__construct();
    }
}