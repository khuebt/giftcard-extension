<?php

/**
 * Created by PhpStorm.
 * User: Khuebt
 * Date: 01/03/16
 * Time: 14:33
 */
class Magebuzz_Giftcard_Block_Adminhtml_Template_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();

        $this->_objectId = 'id';
        $this->_blockGroup = 'giftcard';//wtf
        $this->_controller = 'adminhtml_template';



//        var_dump(Mage::registry('template_data')->getData('is_generated'));
//        $this->_updateButton('delete', 'label', Mage::helper('giftcard')->__('Delete Template'));


        if (Mage::registry('template_data')->getData() && (Mage::registry('template_data')->getData('is_generated')) == 0) {
            $this->_updateButton('save', 'label', Mage::helper('giftcard')->__('Save Item'));
            $this->_addButton('saveandcontinue', array(
                'label' => Mage::helper('adminhtml')->__('Save And Continue Edit'),
                'onclick' => 'saveAndContinueEdit()',
                'class' => 'save',
            ), -100);
            $genData = array(
                'label' => Mage::helper('adminhtml')->__('Generate'),
                'onclick' => 'setLocation(\'' . $this->getUrl('templateadmin/adminhtml_template/generate',array('id' =>Mage::registry('template_data')->getId() )) . '\')',
//            'onclick' => 'localhost',
                'class' => 'generate'
            );
            $this->addButton('Generate', $genData, 3, 0, 'header');
            $this->_updateButton('Generate', 'label', Mage::helper('giftcard')->__('Auto Generate'));
            $this->_updateButton('delete', 'label', Mage::helper('giftcard')->__('Delete Template'));
        } else if(Mage::registry('template_data')->getData() && (Mage::registry('template_data')->getData('is_generated')) == 1){
            $this->_updateButton('delete', 'label', Mage::helper('giftcard')->__('Delete Template'));
            $this->_removeButton('reset');
            $this->_removeButton('save');
        }
        if(!Mage::registry('template_data')->getData()){
            $this->_addButton('saveandcontinue', array(
                'label' => Mage::helper('adminhtml')->__('Save And Continue Edit'),
                'onclick' => 'saveAndContinueEdit()',
                'class' => 'save',
            ), -100);
        }
        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('template_content') == null)
                    tinyMCE.execCommand('mceAddControl', false, 'template_content');
                else
                    tinyMCE.execCommand('mceRemoveControl', false, 'template_content');
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";

    }
//    public function getHeaderText()
//    {
//        if (Mage::registry('membership_data')
//            && Mage::registry('membership_data')->getId()
//        ) {
//            return Mage::helper('membership')->__("Edit Membership '%s'", $this->htmlEscape(Mage::registry('membership_data')->getTitle())
//            );
//        }
//        return Mage::helper('membership')->__('Add Item');
//    }
    public
    function getHeaderText()
    {
        if (Mage::registry('template_data') && Mage::registry('template_data')->getId()
        ) {
            return Mage::helper('giftcard')->__("Edit Item '%s'", $this->htmlEscape(Mage::registry('template_data')->getData('title')));
        } else {
            return Mage::helper('giftcard')->__('Add Item');
        }
    }
}
