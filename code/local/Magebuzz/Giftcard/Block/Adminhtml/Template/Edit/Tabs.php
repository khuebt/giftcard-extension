<?php
class Magebuzz_Giftcard_Block_Adminhtml_Template_Edit_Tabs extends  Mage_Adminhtml_Block_Widget_Tabs

{
    public function __construct()
    {
        parent::__construct();
        $this->setId('template_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('giftcard')->__('Template Information'));
    }
    protected function _beforeToHtml(){
        $this->addTab('form_section', array(
            'label' => Mage::helper('giftcard')->__('Template info'),
            'title' => Mage::helper('giftcard')->__('Thong tin item,hover len moi thay'),
            'content' => $this->getLayout()->createBlock('giftcard/adminhtml_template_edit_tab_form')->toHtml(),
        ));
        return parent::_beforeToHtml();
    }
}
