<?php

class Magebuzz_Giftcard_Block_Adminhtml_Template_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        //lay data tu csdl len form edit
//        if (Mage::getSingleton('adminhtml/session')->getTemplateData()) {
//            $data = $form->setValues(Mage::getSingleton('adminhtml/session')->getTemplateData());
//            Mage::getSingleton('adminhtml/session')->setTemplateData(null);
//        } elseif (Mage::registry('template_data')) {
//            $data = $form->setValues(Mage::registry('template_data')->getData());
//        }


        if (Mage::getSingleton('adminhtml/session')->getTemplateData()) {
            $data = Mage::getSingleton('adminhtml/session')->getTemplateData();
            Mage::getSingleton('adminhtml/session')->setTemplateData(null);
        } elseif (Mage::registry('template_data')) {
            $data = Mage::registry('template_data')->getData();
        }
//        var_dump($data['is_generated']);die();
        $fieldset = $form->addFieldset('giftcard_form', array('legend' => Mage::helper('giftcard')->__('Giftcard information')));
        if(!$data) {
            $fieldset->addField('title', 'text', array(
                'label' => Mage::helper('giftcard')->__('Title'),
                'class' => 'required-entry',
                'required' => true,
                'name' => 'title',
            ));
            $fieldset->addField('giftcode', 'text', array(
                'label' => Mage::helper('giftcard')->__('Gift Code'),
                'required' => true,
                'name' => 'giftcode',
            ));
            $fieldset->addField('status', 'select', array(
                'label' => Mage::helper('giftcard')->__('Status'),
                'name' => 'status',
                'values' => array(
                    array(
                        'value' => 1,
                        'label' => Mage::helper('giftcard')->__('Enabled'),
                    ),
                    array(
                        'value' => 0,
                        'label' => Mage::helper('giftcard')->__('Disabled'),
                    ),
                ),
            ));

            $fieldset->addField('number_of_code', 'text', array(
                'name' => 'number_of_code',
                'label' => Mage::helper('giftcard')->__('Number of code'),
                'required' => true,
            ));

            $fieldset->addField('amount', 'text', array(
                'name' => 'amount',
                'label' => Mage::helper('giftcard')->__('Amount'),
                'title' => Mage::helper('giftcard')->__('Amount'),
                'required' => true,
            ));
        }
        if (isset($data['is_generated']) && $data['is_generated'] == 1) {
            $fieldset->addField('title', 'text', array(
                'label' => Mage::helper('giftcard')->__('Title'),
                'class' => 'required-entry',
                'required' => true,
                'name' => 'title',
                'readonly' => true,
                'disabled' => true,
            ));
            $fieldset->addField('giftcode', 'text', array(
                'label' => Mage::helper('giftcard')->__('Gift Code'),
                'required' => true,
                'readonly' => true,
                'disabled' => true,
                'name' => 'giftcode',
            ));
            $fieldset->addField('status', 'select', array(
                'label' => Mage::helper('giftcard')->__('Status'),
                'name' => 'status',
                'readonly' => true,
                'disabled' => true,
                'values' => array(
                    array(
                        'value' => 1,
                        'label' => Mage::helper('giftcard')->__('Enabled'),
                    ),
                    array(
                        'value' => 0,
                        'label' => Mage::helper('giftcard')->__('Disabled'),
                    ),
                ),
            ));

            $fieldset->addField('number_of_code', 'text', array(
                'name' => 'number_of_code',
                'label' => Mage::helper('giftcard')->__('Number of code'),
                'required' => true,
                'readonly' => true,
                'disabled' => true,
            ));

            $fieldset->addField('is_generated', 'select', array(
                'name' => 'is generated',
                'label' => Mage::helper('giftcard')->__('Is generated'),
                'title' => Mage::helper('giftcard')->__('Is generated'),
                'style' => 'background-color:#b1afad;',
                'readonly' => true,
                'disabled' => true,
                'values' => array(
                    array(
                        'value' => 0,
                        'label' => Mage::helper('giftcard')->__('No'),
                    ),
                    array(
                        'value' => 1,
                        'label' => Mage::helper('giftcard')->__('Yes'),
                    ),
                ),
                'required' => true,
            ));
            $fieldset->addField('amount', 'text', array(
                'name' => 'amount',
                'label' => Mage::helper('giftcard')->__('Amount'),
                'title' => Mage::helper('giftcard')->__('Amount'),
                'required' => true,
                'readonly' => true,
                'disabled' => true,
            ));
            $fieldset->addField('created_time', 'text', array(
                'name' => 'created_time',
                'label' => Mage::helper('giftcard')->__('Create At'),
                'title' => Mage::helper('giftcard')->__('Create At'),
                'readonly' => true,
                'disabled' => true,
                'style' => 'border:none;background-color:#fafafa;'
            ));
//            $fieldset->addField('generate_ssssbutton', 'note', array(
//                'text' => $this->getButtonHtml(
//                    Mage::helper('salesrule')->__('Generate'),
//                    '',
//                    'genserate'
//                )
//            ));
        }
        if (isset($data['is_generated'])&&$data['is_generated'] == 0) {
            $fieldset->addField('title', 'text', array(
                'label' => Mage::helper('giftcard')->__('Title'),
                'class' => 'required-entry',
                'required' => true,
                'name' => 'title',
            ));
            $fieldset->addField('giftcode', 'text', array(
                'label' => Mage::helper('giftcard')->__('Gift Code'),
                'required' => true,
                'name' => 'giftcode',
            ));
            $fieldset->addField('status', 'select', array(
                'label' => Mage::helper('giftcard')->__('Status'),
                'name' => 'status',
                'values' => array(
                    array(
                        'value' => 1,
                        'label' => Mage::helper('giftcard')->__('Enabled'),
                    ),
                    array(
                        'value' => 0,
                        'label' => Mage::helper('giftcard')->__('Disabled'),
                    ),
                ),
            ));

            $fieldset->addField('number_of_code', 'text', array(
                'name' => 'number_of_code',
                'label' => Mage::helper('giftcard')->__('Number of code'),
                'required' => true,
            ));

            $fieldset->addField('is_generated', 'select', array(
                'name' => 'is generated',
                'label' => Mage::helper('giftcard')->__('Is generated'),
                'title' => Mage::helper('giftcard')->__('Is generated'),
                'style' => 'background-color:#b1afad;',
                'readonly' => true,
                'disabled' => true,
                'values' => array(
                    array(
                        'value' => 0,
                        'label' => Mage::helper('giftcard')->__('No'),
                    ),
                    array(
                        'value' => 1,
                        'label' => Mage::helper('giftcard')->__('Yes'),
                    ),
                ),
                'required' => true,
            ));
            $fieldset->addField('amount', 'text', array(
                'name' => 'amount',
                'label' => Mage::helper('giftcard')->__('Amount'),
                'title' => Mage::helper('giftcard')->__('Amount'),
                'required' => true,
            ));
            $fieldset->addField('created_time', 'text', array(
                'name' => 'created_time',
                'label' => Mage::helper('giftcard')->__('Create At'),
                'title' => Mage::helper('giftcard')->__('Create At'),
                'readonly' => true,
                'style' => 'border:none;background-color:#fafafa;'
            ));
            $fieldset->addField('generate_ssssbutton', 'note', array(
                'text' => $this->getButtonHtml(
                    Mage::helper('salesrule')->__('Generate'),
                    '',
                    'genserate'
                )
            ));
        }

        $form->setValues($data);
        return parent::_prepareForm();
    }
}