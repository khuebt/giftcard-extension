<?php

/**
 * Created by PhpStorm.
 * User: Khuebt
 * Date: 08/03/16
 * Time: 11:07
 */
class Magebuzz_Giftcard_Block_Adminhtml_Template_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('templateGrid');
        $this->setDefaultSort('template_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('giftcard/template')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('template_id', array(
            'header' => Mage::helper('giftcard')->__('ID'),
            'align' => 'right',
            'width' => '50px',
            'index' => 'template_id',
        ));
        $this->addColumn('title', array(
            'header' => Mage::helper('giftcard')->__('Title'),
            'align' => 'left',
            'index' => 'title',
        ));
        $this->addColumn('giftcode', array(
            'header' => Mage::helper('giftcard')->__('Giftcode Prefix'),
            'align' => 'left',
            'index' => 'giftcode',
        ));
        $storeId = (int)$this->getRequest()->getParam('store', 0);
        $store = Mage::app()->getStore($storeId);
        $this->addColumn('amount', array(
            'header' => Mage::helper('giftcard')->__('Amount'),
            'align' => 'left',
            'index' => 'amount',
            'type' => 'price',
            'currency_code' => $store->getBaseCurrency()->getCode(),
        ));
//        $this->addColumn('discount_type', array(
//            'header' => Mage::helper('membership')->__('Discount Type'),
//            'align' => 'left',
//            'width' => '80px',
//            'index' => 'discount_type',
//            'type' => 'options',
//            'options' => array(
//                0 => 'Percent',
//                1 => 'Fixed amount',
//            ),
//        ));
        $this->addColumn('number_of_code', array(
            'header' => Mage::helper('giftcard')->__('Number of Code'),
            'width' => '10px',
            'align' => 'right',
            'type' => 'number',
            'index' => 'number_of_code'
        ));
        $this->addColumn('created_time', array(
            'header' => Mage::helper('giftcard')->__('Created At'),
            'type'=>'datetime',
            'index' => 'created_time'
        ));
        $this->addColumn('is_generated', array(
            'header' => Mage::helper('giftcard/template')->__('Is Generated'),
            'align' => 'left',
            'width' => '80px',
            'index' => 'is_generated',
            'type' => 'options',
            'options' => array(
                0 => 'No',
                1 => 'Yes',
            ),
        ));
        $this->addColumn('status', array(
            'header' => Mage::helper('giftcard')->__('Status'),
            'align' => 'left',
            'width' => '100px',
            'index' => 'status',
            'type' => 'options',
            'options' => array(
                0 => 'Disabled',
                1 => 'Enabled',
            ),
        ));


        $this->addColumn('action',
            array(
                'header' => Mage::helper('giftcard')->__('Action'),
                'width' => '80',
                'type' => 'action',
                'getter' => 'getId',
                'actions' => array(
                    array(
                        'caption' => Mage::helper('giftcard')->__('Edit'),
                        'url' => array('base' => '*/*/edit'),
                        'field' => 'id'
                    )
                ),
                'filter' => false,
                'sortable' => false,
                'index' => 'stores',
                'is_system' => true,
            ));
        $this->addExportType('*/*/exportCsv', Mage::helper('giftcard')->__('CSV'));
        $this->addExportType('*/*/exportXml', Mage::helper('giftcard')->__('XML'));
        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('template_id');
        $this->getMassactionBlock()->setFormFieldName('template');
        $this->getMassactionBlock()->addItem('delete', array(
            'label' => Mage::helper('giftcard')->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
            'confirm' => Mage::helper('giftcard/template')->__('Are you sure?')
        ));
        $statuses = Mage::getSingleton('giftcard/status')->getOptionArray();
        // var_dump($statuses);
//        array_unshift($statuses, array('label' => '', 'value' =>
//            ''));
        $this->getMassactionBlock()->addItem('status', array(
            'label' => Mage::helper('giftcard')->__('Change status'),
            'url' => $this->getUrl('*/*/massStatus', array('_current' => true)),
            'additional' => array(
                'visibility' => array(
                    'name' => 'status',
                    'type' => 'select',
                    'class' => 'required-entry',
                    'label' => Mage::helper('giftcard')->__('Status'),
                    'values' => $statuses
                )
            )
        ));
        return $this;
    }
}
