<?php
class Magebuzz_Giftcard_Block_Adminhtml_Giftcard extends Mage_Adminhtml_Block_Widget_Grid_Container{
    public function __construct(){
        //  die('1');
        $this->_controller = 'adminhtml_giftcard';
        $this->_blockGroup = 'giftcard';
        $this->_headerText = Mage::helper('giftcard')->__('Giftcard Manager');
        $this->_addButtonLabel = Mage::helper('giftcard')->__('Add Item');
        parent::__construct();
    }
}