<?php

class Magebuzz_Custommember_Block_Sales_Invoice_Discount extends Mage_Core_Block_Template
{
    public function initTotals()
    {
        $totalsBlock = $this->getParentBlock();
        $invoice = $totalsBlock->getInvoice();

        if ($invoice->getCustommemberDiscount()) {
            $totalsBlock->addTotal(new Varien_Object(array(
                'code' => 'code de lam gi',
                'label' => $this->__('Deal invoice frontend'),
                'value' => $invoice->getCustommemberDiscount(),
            )), 'subtotal');
        }
    }
}
