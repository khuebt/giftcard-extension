<?php

class Magebuzz_Custommember_Block_Adminhtml_Totals_Invoice_Discount extends Mage_Adminhtml_Block_Sales_Order_Totals_Item
{
    public function initTotals()
    {
        $totalsBlock = $this->getParentBlock();
        $invoice = $totalsBlock->getInvoice();

        if ($invoice->getCustommemberDiscount()) {
            $totalsBlock->addTotal(new Varien_Object(array(
                'code' => $this->getNameInLayout(),
                'label' => $this->__('Deal in Backend'),
                'value' => $invoice->getCustommemberDiscount(),
            )), 'subtotal');
        }
    }

}