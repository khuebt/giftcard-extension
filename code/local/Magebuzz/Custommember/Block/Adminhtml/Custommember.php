<?php
/**
 * Created by PhpStorm.
 * User: Khuebt
 * Date: 29/02/16
 * Time: 16:30
 */
class Magebuzz_Custommember_Block_Adminhtml_Custommember extends Mage_Adminhtml_Block_Widget_Grid_Container{
    public function __construct(){
        $this->_controller = 'adminhtml_custommember';
        $this->_blockGroup = 'custommember';
        $this->_headerText = Mage::helper('custommember')->__('ItemManager');
        $this->_addButtonLabel = Mage::helper('custommember')->__('Them Item moi');
        parent::__construct();
    }
}