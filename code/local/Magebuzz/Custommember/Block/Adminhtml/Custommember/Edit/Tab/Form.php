<?php

/**
 * Created by PhpStorm.
 * User: Khuebt
 * Date: 01/03/16
 * Time: 15:18
 */
class Magebuzz_Custommember_Block_Adminhtml_Custommember_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * prepare tab form's information
     *
     * @return Magebuzz_Test_Block_Adminhtml_Test_Edit_Tab_Form
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);

        if (Mage::getSingleton('adminhtml/session')->getCustommemberData()) {
            $data = Mage::getSingleton('adminhtml/session')->getCustommemberData();
            Mage::getSingleton('adminhtml/session')->setCustommemberData(null);
        } elseif (Mage::registry('custommember_data')) {
            $data = Mage::registry('custommember_data')->getData();
        }
        var_dump($data);
        $fieldset = $form->addFieldset('custommember_form', array(
            'legend' => Mage::helper('custommember')->__('Item information')
        ));
        $fieldset->addType('custom_field', 'Magebuzz_Custommember_Block_Adminhtml_Custommember_Edit_Tab_Field_Custom');

        $fieldset->addField('custom_field', 'custom_field', array(
            'label'     => Mage::helper('custommember')->__('Checkboxs'),
            'name'      => 'Checkbox',
            'custom1'  => 'Custom1 Value',
            'custom2'  => 'Custom2 Value',
            'value' => 'value1'
        ));


        $fieldset->addField('name', 'text', array(
            'label' => Mage::helper('custommember')->__('Ten cua member'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'name',//name chinh la field trong table
            'disabled' => false,
            //'readonly' => true,
            'after_element_html' => 'Nhap ten member',
        ));
        $fieldset->addField('email', 'text', array(
            'label' => Mage::helper('custommember')->__('Email cua member'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'email',//name chinh la field trong table
            'disabled' => false,
            //'readonly' => true,
            'after_element_html' => 'Nhap email member',
        ));

        $fieldset->addField('tel', 'text', array(
            'label' => Mage::helper('custommember')->__('Tel cua member'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'tel',//name chinh la field trong table
            'disabled' => false,
            //'readonly' => true,
            'after_element_html' => 'Nhap so dien thoai member',
        ));

        $fieldset->addField('birthday', 'date', array(
            'label' => Mage::helper('custommember')->__('Date'),
            'name'=>'birthday',
            'after_element_html' => 'Birthday',
            'image' => $this->getSkinUrl('images/grid-cal.gif'),
            'format' => Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_MEDIUM)
        ));
//        $fieldset->addField('spuathich', 'text', array(
//            'label' => Mage::helper('custommember')->__('San pham ua thich'),
//            //'class' => 'required-entry',
//            'required' => false,
//            'name' => 'product_favorite',//name chinh la field trong table
//            'disabled' => false,
//            //'readonly' => true,
//            'after_element_html' => 'Nhap sp ua thich cua member',
//        ));

        $fieldset->addField('select', 'select', array(
            'label' => Mage::helper('custommember')->__('Select'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'title',
            'onclick' => "",
            'onchange' => "",
            'values' => array('0'=>'Please Select..','1' => 'Option1','2' => 'Option2', '3' => 'Option3'),
            'disabled' => false,
            'readonly' => false,
            'after_element_html' => 'Drop down',
            'tabindex' => 1
        ));







        $form->setValues($data);
        return parent::_prepareForm();
    }
}
