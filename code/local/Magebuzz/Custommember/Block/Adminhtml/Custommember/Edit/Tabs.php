<?php
/**
 * Created by PhpStorm.
 * User: Khuebt
 * Date: 01/03/16
 * Time: 14:35
 */
class Magebuzz_Custommember_Block_Adminhtml_Custommember_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('custommember_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('custommember')->__('Information Member'));
    }

    /**
     * prepare before render block to html
     *
     * @return Magebuzz_Test_Block_Adminhtml_Test_Edit_Tabs
     */
    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
            'label'     => Mage::helper('custommember')->__('Edit Member'),
            'title'     => Mage::helper('custommember')->__('Edit Member'),
            'content'   => $this->getLayout()
                ->createBlock('custommember/adminhtml_custommember_edit_tab_form')
                ->toHtml()
        ));

        return parent::_beforeToHtml();
    }
}
