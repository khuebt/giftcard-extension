<?php
/**
 * Created by PhpStorm.
 * User: Khuebt
 * Date: 01/03/16
 * Time: 17:16
 */
class Magebuzz_Custommember_Block_Adminhtml_Custommember_Renderer_Email
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {
    /* Render Grid Column*/
    public function render(Varien_Object $row)      {
        if($row->getEmail())
            return sprintf('<a href="mailto:%s">%s</a>',
                $row->getEmail(),
                $row->getEmail());
    }
}
