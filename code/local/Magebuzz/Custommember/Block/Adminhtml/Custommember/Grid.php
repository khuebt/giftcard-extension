<?php

/**
 * Created by PhpStorm.
 * User: Khuebt
 * Date: 29/02/16
 * Time: 16:36
 */
class Magebuzz_Custommember_Block_Adminhtml_Custommember_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('custommemberGrid');
        $this->setDefaultSort('custommember_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));//tao tham so cho url
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('custommember/member')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }


    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('custommember_id');
        $this->getMassactionBlock()->setFormFieldName('custommember_id');
        $this->getMassactionBlock()->addItem('delete', array(
            'label' => Mage::helper('custommember')->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
            'confirm' => Mage::helper('custommember')->__('Are you sure?')
        ));
        $this->getMassactionBlock()->addItem('Addtospam', array(
            'label' => Mage::helper('custommember')->__('Spam ne'),
            'url' => $this->getUrl('*/*/massAddtoSpammer'),
            'confirm' => Mage::helper('custommember')->__('Are you sure?')
        ));
        return $this;
    }

    protected function _prepareColumns()
    {
        $this->addColumn('member_id', array(
            'header' => Mage::helper('custommember')->__('ID'),
            'align' => 'right',
            'width' => '50px',
            'index' => 'custommember_id',
            'type' => 'number',
        ));
        $this->addColumn('name', array(
            'header' => Mage::helper('custommember')->__('Name'),
            'align' => 'left',
            'index' => 'name',
        ));
        $this->addColumn('email', array(
            'header' => Mage::helper('custommember')->__('Email'),
            'width' => '300px',
            'index' => 'email',
            //'type' => 'email',
            'renderer' => 'custommember/adminhtml_custommember_renderer_email',
        ));

        $this->addColumn('birthday', array(
            'header' => Mage::helper('custommember')->__('Birthday'),
            'width' => '200px',
            'index' => 'birthday',
        ));

        $this->addColumn('sodienthoai', array(
            'header' => Mage::helper('custommember')->__('Telephone'),
            'width' => '200px',
            'index' => 'tel',
        ));
//        $this->addColumn('product_favorite', array(
//            'header' => Mage::helper('custommember')->__('San pham ua thich'),
//            'width' => '200px',
//            'index' => 'product_favorite',
//        ));
        $this->addExportType('*/*/exportCsv', Mage::helper('custommember')->__('CSV'));
        $this->addExportType('*/*/exportXml', Mage::helper('custommember')->__('XML'));

    }
}