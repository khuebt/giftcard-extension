<?php
/**
 * Created by PhpStorm.
 * User: Khuebt
 * Date: 01/03/16
 * Time: 14:33
 */
class Magebuzz_Custommember_Block_Adminhtml_Custommember_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
        $this->_objectId = 'custommember_id';
        $this->_blockGroup = 'custommember';
        $this->_controller = 'adminhtml_custommember';

        $this->_updateButton('save', 'label', Mage::helper('custommember')->__('Save Item'));
        $this->_updateButton('delete', 'label', Mage::helper('custommember')->__('Delete Item'));

        $this->_addButton('saveandcontinue', array(
            'label'        => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'    => 'saveAndContinueEdit()',
            'class'        => 'save',
        ), -100 );

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('custommember_content') == null)
                    tinyMCE.execCommand('mceAddControl', false, 'custommember_content');
                else
                    tinyMCE.execCommand('mceRemoveControl', false, 'custommember_content');
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";

    }
    public function getHeaderText()
    {
        if (Mage::registry('custommember_data')
            && Mage::registry('custommember_data')->getId()
        ) {
            return Mage::helper('custommember')->__("Edit Member '%s'",
                $this->htmlEscape(Mage::registry('custommember_data')->getTitle())
            );
        }
        return Mage::helper('custommember')->__('Add Item');
    }
}
