<?php

class Magebuzz_Custommember_Model_Quote_Address_Total_Custommember extends Mage_Sales_Model_Quote_Address_Total_Abstract
{
    public function collect(Mage_Sales_Model_Quote_Address $address)
    {
      // die('11');
        $baseDiscount = 5;
        $discount = 5;
        $address->setTestDiscount(-$baseDiscount);
      //  $address->setTestDiscount(-$discount);
        $address->setBaseGrandTotal($address->getBaseGrandTotal() - $baseDiscount);
        //$address->setGrandTotal($address->getGrandTotal() - $discount);
        return $this;
    }

    public function fetch(Mage_Sales_Model_Quote_Address $address)
    {

        $amount = $address->getTestDiscount();
        $title = Mage::helper('custommember')->__('Giam gia');
        if ($amount != 0) {
            $address->addTotal(array(
                'code' => $this->getCode(),
                'title' => $title,
                'value' => $amount
            ));
        }
      //  var_dump($address);
        return $this;
    }
}