<?php
/**
 * Created by PhpStorm.
 * User: Khuebt
 * Date: 02/03/16
 * Time: 0:40
 */

echo 'Running This upgarde: '.get_class($this)."\n <br /> \n";
$installer = $this;

$installer->startSetup();
$installer->run("
ALTER TABLE `{$installer->getTable('custommember')}` ADD `favorite_product` INT(11) UNSIGNED NULL DEFAULT NULL;
    ");

$installer->endSetup();