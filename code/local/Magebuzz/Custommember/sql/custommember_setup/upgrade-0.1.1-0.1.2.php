<?php
echo 'Running This upgarde: '.get_class($this)."\n <br /> \n";
$installer = $this;
$installer->run("

ALTER TABLE {$this->getTable('sales/order')}

ADD COLUMN `test_discount` decimal(12,4) default NULL,

ADD COLUMN `base_test_discount` decimal(12,4) default NULL;

");
$installer->endSetup();