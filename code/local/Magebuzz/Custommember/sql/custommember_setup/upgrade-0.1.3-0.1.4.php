<?php
echo 'Running This upgarde: '.get_class($this)."\n <br /> \n";
$installer = $this;
$installer->run("

ALTER TABLE {$this->getTable('sales/order_item')}
ADD COLUMN `custommember_discount` decimal(12,4) NOT NULL default '0',
ADD COLUMN `base_custommember_discount` decimal(12,4) NOT NULL default '0';

ALTER TABLE {$this->getTable('sales/invoice')}
ADD COLUMN `custommember_discount` decimal(12,4) NOT NULL default '0',
ADD COLUMN `base_custommember_discount` decimal(12,4) NOT NULL default '0';


");
$installer->endSetup();