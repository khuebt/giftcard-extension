<?php
/**
 * Created by PhpStorm.
 * User: Khuebt
 * Date: 29/02/16
 * Time: 6:39
 */
/* @var $installer Mage_Core_Model_Resource_Setup */

echo 'Running This installer: '.get_class($this)."\n <br /> \n";
$installer = $this;

$installer->startSetup();
$installer->run("
        CREATE TABLE `{$installer->getTable('custommember')}` (
        `custommember_id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
        `name`  varchar(50) NULL ,
        `email`  varchar(50) NULL ,
        `birthday`  date NULL ,
        `tel`  integer(50) NULL ,
        PRIMARY KEY (`custommember_id`)
        )
        ;
");

$installer->endSetup();